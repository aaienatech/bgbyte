import React from "react";
import {View,Text,Image,Button,TouchableOpacity,StyleSheet} from "react-native";
import Onboarding from 'react-native-onboarding-swiper';


const Dots =()=>{

  return (
    <View 
      style={{
        width:8,
        borderRadius:5,
        height:8,
        marginHorizontal:3,
        backgroundColor:"white"
      }}
    />
  )
}

const Skip = ({...props})=>(
  <Button 
    title="Skip"
    color="green"
    {...props}
  />
);

const Next = ({...props})=>(
  <Button 
    title="Next"
    color="green"
    {...props}
  />
);

const Done = ({...props})=>(
<TouchableOpacity
  style={{marginHorizontal:10,}}
  {...props }
   >
  <Text style={{fontSize:18,fontWeight:"bold",color:"white"}}>Done</Text>
</TouchableOpacity>
);

const Onboardings =({navigation})=>{
return (
<Onboarding
SkipButtonComponent={Skip}
NextButtonComponent={Next}
DoneButtonComponent={Done}
DotComponent={Dots}
onSkip={()=>navigation.replace("Upload_img")}
onDone={()=>navigation.replace("Upload_img")}
  pages={[
    {
      id:1,
      title:"1",
      backgroundColor: '#14466b',
      image: <Image style={styles.image}
      source={require("../assets/onboarding-img10.jpeg")}/>,
    },
    {
      id:2,
      backgroundColor: '#2BCBCD',
      image: <Image style={styles.image}
      source={require("../assets/onboarding-img11.jpg")}/>,
    },
    {
      id:3,
      backgroundColor: 'green',
      image: <Image style={styles.image}
      source={require("../assets/onboarding-img13.jpg")}/>,
    },
  ]}
/>
)
}
export default Onboardings;

const styles = StyleSheet.create({
  container: {
    flex:1,
    justifyContent:"center",
  },
  image: {
    width:370,
    height:700,
    resizeMode:"cover"
  }
});