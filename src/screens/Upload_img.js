import React, { useState } from 'react';
// Import required components
import {
  SafeAreaView,
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Image,
  Platform,
  PermissionsAndroid,
  Modal,
} from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import RNFetchBlob from 'rn-fetch-blob';
import Share from "react-native-share"



// Import Image Picker
// import ImagePicker from 'react-native-image-picker';
import {
  launchCamera,
  launchImageLibrary
} from 'react-native-image-picker';

const Upload_img = () => {
  const [image, setImage] = useState({});
  const [modalVisible, setModalVisible] = useState(false);



  const requestCameraPermission = async () => {
    if (Platform.OS === 'android') {
      try {
        const granted = await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.CAMERA,
          {
            title: 'Camera Permission',
            message: 'App needs camera permission',
          },
        );
        // If CAMERA Permission is granted
        return granted === PermissionsAndroid.RESULTS.GRANTED;
      } catch (err) {
        console.warn(err);
        return false;
      }
    } else return true;
  };

  const requestExternalWritePermission = async () => {
    if (Platform.OS === 'android') {
      try {
        const granted = await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
          {
            title: 'External Storage Write Permission',
            message: 'App needs write permission',
          },
        );
        // If WRITE_EXTERNAL_STORAGE Permission is granted
        return granted === PermissionsAndroid.RESULTS.GRANTED;
      } catch (err) {
        console.warn(err);
        alert('Write permission err', err);
      }
      return false;
    } else return true;
  };

  const captureImage = async (type) => {
    let options = {
      mediaType: type,
      maxWidth: 300,
      maxHeight: 550,
      quality: 1,
      videoQuality: 'low',
      durationLimit: 30, //Video max duration in seconds
      saveToPhotos: true,
      
    };
    let isCameraPermitted = await requestCameraPermission();
    let isStoragePermitted = await requestExternalWritePermission();
    if (isCameraPermitted && isStoragePermitted) {
      launchCamera(options, (response,) => {
        console.log('Response = ', response);

        if (response.didCancel) {
          alert('You cancelled camera');
          return;
        } else if (response.errorCode == 'camera_unavailable') {
          alert('Camera not available on device');
          return;
        } else if (response.errorCode == 'permission') {
          alert('Permission not satisfied');
          return;
        } else if (response.errorCode == 'others') {
          alert(response.errorMessage);
          return;
        }
        console.log('base64 -> ', response.base64);
        console.log('uri -> ', response.uri);
        console.log('width -> ', response.width);
        console.log('height -> ', response.height);
        console.log('fileSize -> ', response.fileSize);
        console.log('type -> ', response.type);
        console.log('fileName -> ', response.fileName);
      
        setImage(response);
      });
    }
  };

  const chooseFile = (type) => {
    let options = {
      mediaType: type,
      maxWidth: 300,
      maxHeight: 550,
      quality: 1,
    };
    launchImageLibrary(options, (response) => {
      console.log('Response = ', response);

      if (response.didCancel) {
        alert('You cancelled camera');
        return;
      } else if (response.errorCode == 'camera_unavailable') {
        alert('Camera not available on device');
        return;
      } else if (response.errorCode == 'permission') {
        alert('Permission not satisfied');
        return;
      } else if (response.errorCode == 'others') {
        alert(response.errorMessage);
        return;
      }
      console.log('base64 -> ', response.base64);
      console.log('uri -> ', response.uri);
      console.log('width -> ', response.width);
      console.log('height -> ', response.height);
      console.log('fileSize -> ', response.fileSize);
      console.log('type -> ', response.type);
      console.log('fileName -> ', response.fileName);
      setImage(response);
    });
  };



 const onShare = async () => {
   const shareOptions={
     message:"",
     url:image.uri
   }
    try {
      const ShareResponse = await Share.open(shareOptions);
      console.log(JSON.stringify(ShareResponse));
    } catch (error) {
      console.log('Error =>',error)
    }
    
  };

  
  const checkPermission = async () => {
    
    // Function to check the platform
    // If iOS then start downloading
    // If Android then ask for permission

    if (Platform.OS === 'ios') {
      downloadImage();
    } else {
      try {
        const granted = await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
          {
            title: 'Storage Permission Required',
            message:
              'App needs access to your storage to download Photos',
          }
        );
        if (granted === PermissionsAndroid.RESULTS.GRANTED) {
          // Once user grant the permission start downloading
          console.log('Storage Permission Granted.');
          downloadImage();
        } else {
          // If permission denied then show alert
          alert('Storage Permission Not Granted');
        }
      } catch (err) {
        // To handle permission related exception
        console.warn(err);
      }
    }
  };


  const downloadImage = () => {
    // Main function to download the image
    // To add the time suffix in filename
    let date = new Date();
    // Image URL which we want to download
    let image_URL ='https://raw.githubusercontent.com/AboutReact/sampleresource/master/gift.png';    
    // Getting the extention of the file
    let ext = getExtention(image_URL);
    ext = '.' + ext[0];
    // Get config and fs from RNFetchBlob
    // config: To pass the downloading related options
    // fs: Directory path where we want our image to download
    const { config, fs } = RNFetchBlob;
    let PictureDir = fs.dirs.PictureDir;
    let options = {
      fileCache: true,
      addAndroidDownloads: {
        // Related to the Android only
        useDownloadManager: true,
        notification: true,
        path:
          PictureDir +
          '/image_' + 
          Math.floor(date.getTime() + date.getSeconds() / 2) +
          ext,
        description: 'Image',
      },
    };
    config(options)
      .fetch('GET', image_URL)
      .then(res => {
        // Showing alert after successful downloading
        console.log('res -> ', JSON.stringify(res));
        alert('Image Downloaded Successfully.');
      });
  };

  const getExtention = filename => {
    // To get the file extension
    return /[.]/.exec(filename) ?
             /[^.]+$/.exec(filename) : undefined;
  };


  return (
    <SafeAreaView style={{ flex: 1 }}>
      <Modal
        animationType="slide"
        visible={modalVisible}
        transparent={false}
        onRequestClose={() => {

          setModalVisible(!modalVisible);
        }}
      >
        <View style={{ flex: 1 }}>
          {
            (image.uri != null) ?
              <>

                <View >
                  <TouchableOpacity onPress={() => setModalVisible(false)}>
                    <Text style={{ marginLeft: 20, marginTop: 20, marginBottom: 20 }}><Ionicons name="arrow-back-sharp" size={17} /> BACK                                      </Text>
                  </TouchableOpacity>
                  <Image
                    source={{ uri: image.uri }}
                    style={{ width: "100%", height: 500, }}
                  />
                </View>
                <View style={{ marginTop: 20, alignSelf: "center", flexDirection: "row" }}>
                  <TouchableOpacity onPress={downloadImage}>
                    <Text style={{ alignSelf: "center", backgroundColor: "#14466b", color: "white", height: 40, width: 100, textAlign: "center", marginRight: 60, borderRadius: 5, paddingTop: 8, fontWeight: "bold", fontSize: 16 }}>Save</Text>
                  </TouchableOpacity>

                  <TouchableOpacity onPress={onShare}>
                    <Text style={{ alignSelf: "center", backgroundColor: "#14466b", color: "white", height: 40, width: 100, textAlign: "center", borderRadius: 5, paddingTop: 8, fontWeight: "bold", fontSize: 16 }}>Share</Text>
                  </TouchableOpacity>
                </View>
              </>
              :
              <>
                <View>
                  <TouchableOpacity onPress={() => setModalVisible(false)}>
                    <Text style={{ marginLeft: 20, marginTop: 20, fontWeight: "bold", marginBottom: 20 }}> <Ionicons name="arrow-back-sharp" size={20} style={{ fontWeight: "bold", marginTop: 20 }} /> BACK</Text>
                  </TouchableOpacity>
                  <Image
                    source={require("../assets/main-logo2.jpeg")}
                    style={{ width: "100%", height: "100%", resizeMode: "contain", marginTop: -40 }}
                  />
                </View>
              </>
          }
        </View>

      </Modal>

      <View style={styles.container}>

        <View>
          <Text style={styles.titleText}>
            Upload Your Image
      </Text>
        </View>

        {/* <Image
          source={{
            uri: 'data:image/jpeg;base64,' + image1.data,
          }}
          style={styles.imageStyle}
        /> */}
        {
          (image.uri != null) ?
            <TouchableOpacity onPress={() => setModalVisible(true)}>
              <Image
                source={{ uri: image.uri }}
                onPress={() => alert("hi")}
                style={styles.imageStyle}
              />
            </TouchableOpacity> :

            <TouchableOpacity
              onPress={() => setModalVisible(true)}>
              <Image
                source={require("../assets/main-logo2.jpeg")}
                style={styles.imageStyle}
              />
            </TouchableOpacity>

        }


        {/* <Text style={styles.textStyle}>{image1.uri}</Text> */}

        <TouchableOpacity
          activeOpacity={0.5}
          style={styles.buttonStyle}
          onPress={() => captureImage('photo')}>
          <Text style={styles.textStyle}>
            Camera
          </Text>
        </TouchableOpacity>

        <TouchableOpacity
          activeOpacity={0.5}
          style={styles.buttonStyle}
          onPress={() => chooseFile('photo')}>
          <Text style={styles.textStyle}>Gallery</Text>
        </TouchableOpacity>
      </View>
    </SafeAreaView>
  );
};

export default Upload_img;

const styles = StyleSheet.create({
  container: {
    flex: 1,

    backgroundColor: '#14466b',
    alignItems: 'center',
  },
  titleText: {
    fontSize: 20,
    fontWeight: 'bold',
    textAlign: 'center',
    backgroundColor: "white",
    padding: 10,
    alignSelf: "center",
    marginTop: 50,
    borderRadius: 10,
    marginBottom: 50
  },
  textStyle: {
    padding: 10,
    color: 'black',
    textAlign: 'center',
    fontWeight: "bold",
    fontSize: 16,
  },
  buttonStyle: {
    alignItems: 'center',
    backgroundColor: 'white',
    padding: 5,
    marginVertical: 10,
    width: 250,
    borderRadius: 10,


  },
  imageStyle: {
    width: 300,
    height: 300,
    margin: 5,
    borderRadius: 10,
    marginBottom: 40
  },
});