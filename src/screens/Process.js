import React, {useState} from 'react';
import Ionicons from 'react-native-vector-icons/Ionicons';
import Entypo from 'react-native-vector-icons/Entypo';
import {
  SafeAreaView,
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Image,
  Platform,
  PermissionsAndroid,
  Modal,
  ScrollView,
  ImageBackground,
  ActivityIndicator,
  ToastAndroid,
} from 'react-native';
import RNFetchBlob from 'rn-fetch-blob';
import {LoginButton, AccessToken} from 'react-native-fbsdk';
import Share from 'react-native-share';
import {useRoute} from '@react-navigation/native';
import {useEffect} from 'react';

const Process = () => {
  const route = useRoute();

  const [base64Image, setBase64Image] = useState(null);
  const [image, setImage] = useState(route.params?.image);

  const onShare = async () => {
    const shareOptions = {
      message: '',
      url: base64Image,
    };
    try {
      const ShareResponse = await Share.open(shareOptions);
      console.log(JSON.stringify(ShareResponse));
    } catch (error) {
      console.log('Error =>', error);
    }
  };

  const checkPermission = async () => {
    // Function to check the platform
    // If iOS then start downloading
    // If Android then ask for permission

    if (Platform.OS === 'ios') {
      downloadImage();
    } else {
      try {
        const granted = await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
          {
            title: 'Storage Permission Required',
            message: 'App needs access to your storage to download Photos',
          },
        );
        if (granted === PermissionsAndroid.RESULTS.GRANTED) {
          // Once user grant the permission start downloading
          console.log('Storage Permission Granted.');
          downloadImage();
        } else {
          // If permission denied then show alert
          alert('Storage Permission Not Granted');
        }
      } catch (err) {
        // To handle permission related exception
        console.warn(err);
      }
    }
  };

  const downloadImage = () => {
    // Main function to download the image
    // To add the time suffix in filename
    var Base64Code = base64Image.split('data:image/jpeg;base64,'); //base64Image is my image base64 string

    // Get config and fs from RNFetchBlob
    // config: To pass the downloading related options
    // fs: Directory path where we want our image to download
    const {config, fs} = RNFetchBlob;
    let PictureDir = fs.dirs.PictureDir;

    const dirs = RNFetchBlob.fs.dirs;

    var path = dirs.DCIMDir + `/image-${Date.now().toString()}.png`;

    RNFetchBlob.fs.writeFile(path, Base64Code[1], 'base64').then(res => {
      console.log('File : ', res);
      ToastAndroid.show(`image is downloaded in ${path}`, ToastAndroid.LONG);
    });

    // let options = {
    //   fileCache: true,
    //   addAndroidDownloads: {
    //     // Related to the Android only
    //     useDownloadManager: true,
    //     notification: true,
    //     path:
    //       PictureDir +
    //       '/image_' +
    //       Math.floor(date.getTime() + date.getSeconds() / 2) +
    //       '.png',
    //     description: 'Image',
    //   },
    // };
    // config(options)
    //   .fetch('GET', base64Image)
    //   .then(res => {
    //     // Showing alert after successful downloading
    //     console.log('res -> ', JSON.stringify(res));
    //     alert('Image Downloaded Successfully.');
    //   });
  };

  const getExtention = filename => {
    // To get the file extension
    return /[.]/.exec(filename) ? /[^.]+$/.exec(filename) : undefined;
  };
  useEffect(() => {
    removeBG();
  }, []);

  const removeBG = async () => {
    let body = new FormData();
    body.append('image', {
      uri: image.uri.replace('file://', ''),
      name: image.fileName,
      type: image.type,
    });
    // body.append('Content-Type', 'image/png');

    fetch('http://35.153.51.213/backgroundRemoval', {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data',
      },
      body: body,
    })
      .then(res => res.json())
      .then(res => {
        console.log('data:image/jpeg;base64,' + res.data);
        setBase64Image('data:image/jpeg;base64,' + res.data);
      })
      .catch(e => console.log(e))
      .done();
  };
  return (
    <ImageBackground
      source={require('../assets/bgc.jpg')}
      resizeMode="cover"
      style={styles.image}>
      <View
        style={{
          flex: 0.7,
        }}>
        {base64Image ? (
          <Image
            source={{
              uri: base64Image,
            }}
            style={{flex: 1, height: '100%', width: '100%'}}
            resizeMode="contain"
          />
        ) : (
          <>
            <Image
              style={{flex: 1, height: '100%', width: '100%'}}
              resizeMode="contain"
              source={{uri: image.uri}}
            />
          </>
        )}
      </View>
      <View
        style={{
          flex: 0.3,
          flexDirection: 'column',
        }}>
        {!base64Image && (
          <ActivityIndicator
            style={{
              marginBottom: 50,
            }}
            size="large"
            color="#14466b"
          />
        )}
        <TouchableOpacity onPress={downloadImage}>
          <Text
            style={{
              backgroundColor: '#14466b',
              color: 'white',
              textAlign: 'center',
              borderRadius: 5,
              paddingHorizontal: 20,
              fontWeight: 'bold',
              fontSize: 20,
              paddingVertical: 10,
              marginHorizontal: 60,
              marginTop: 20,
            }}>
            Save
          </Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={onShare}>
          <Text
            style={{
              backgroundColor: '#14466b',
              color: 'white',
              textAlign: 'center',
              borderRadius: 5,
              paddingHorizontal: 20,
              fontWeight: 'bold',
              fontSize: 20,
              paddingVertical: 10,
              marginHorizontal: 60,
            }}>
            Share
          </Text>
        </TouchableOpacity>
      </View>
    </ImageBackground>
  );
};

export default Process;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: 20,
  },
  imageStyle: {
    height: 350,
    width: null,
    flex: 1,
    justifyContent: 'center',
  },
  buttonStyle: {
    alignItems: 'center',
    backgroundColor: 'white',
    padding: 5,
    marginVertical: 10,
    borderRadius: 10,
    flexDirection: 'row',
    justifyContent: 'center',
  },
  image: {
    flex: 1,
    justifyContent: 'center',
  },
});
