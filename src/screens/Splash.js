import React from "react";
import { useEffect } from "react";
import {View,Image,StyleSheet,Animated} from "react-native";

const Splash =({ navigation })=>{

    const width = new Animated.Value(350);
    const height = new Animated.Value(350);

    useEffect(() => {
        // start animation from here 
        Animated.timing(
          width, // The animated value to drive
          {
            toValue:360, // Animate to opacity: 1 (opaque)
            duration: 350, // Make it take a while
            useNativeDriver: false,
          },
        ).start(); // Starts the animation
        Animated.timing(
          height, // The animated value to drive
          {
            toValue: 750, // Animate to opacity: 1 (opaque)
            duration: 8000, // Make it take a while
            useNativeDriver: false,
          },
        ).start(); // Starts the animation and end of animation part
     
        // set time out start here 
      setTimeout(()=>{
            navigation.replace('Onboardings')
         },2000);

    }, []);
    

return(
        <View>
        <Animated.Image
        source={require("../assets/main-logo2.jpeg")}
        style={{
          width: width,
          height: height,
          marginTop:140
             }}
      />
    </View>
    
)
}

export default Splash;

const styles = StyleSheet.create({
    container: {
        flex:1,
      backgroundColor: '#fff',
      position: 'relative',
    },
  });