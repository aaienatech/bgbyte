import React, {useState} from 'react';
import {
  Button,
  StyleSheet,
  ImageBackground,
  View,
  Image,
  TouchableOpacity,
  Text,
} from 'react-native';
import {useState} from 'react';

const Next = ({navigation}) => {
  const [image, setImage] = useState(route.params?.image);

  return (
    <ImageBackground
      source={require('../assets/bgc.jpg')}
      resizeMode="cover"
      style={styles.image}>
      <Image
        style={{flex: 0.8, height: '100%', width: '100%'}}
        resizeMode="contain"
        source={{uri: image.uri}}
      />
      <View style={{justifyContent: 'center', alignItems: 'center'}}>
        <TouchableOpacity
          onPress={() => {
            navigation.navigate('Process', {
              image: image,
            });

            console.log('Touchable  presssed...');
          }}
          style={styles.buttonStyle}>
          <Text style={styles.textStyle}>Next</Text>
        </TouchableOpacity>
      </View>
    </ImageBackground>
  );
};

export default Next;

const styles = StyleSheet.create({
  image: {
    flex: 1,
    justifyContent: 'center',
  },
  buttonStyle: {
    alignItems: 'center',
    backgroundColor: 'white',
    padding: 5,
    marginVertical: 10,
    width: 250,
    borderRadius: 10,
    flexDirection: 'row',
    justifyContent: 'center',
  },
  textStyle: {
    padding: 10,
    color: 'black',
    textAlign: 'center',
    fontWeight: 'bold',
    fontSize: 16,
  },
});
