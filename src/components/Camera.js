import React, {useState} from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Image,
  Button,
} from 'react-native';

// importing camera
import {RNCamera} from 'react-native-camera';

//will show the loading until camera loads
const PendingView = () => (
  <View
    style={{
      flex: 1,
      justifyContent: 'center',

      alignItems: 'center',
    }}>
    <Text
      style={{
        fontSize: 30,
        color: 'red',
      }}>
      Loading...
    </Text>
  </View>
);

const Camera = () => {
  // to store the captured image
  const [image, setImage] = useState(null);

  // will take the picture
  const takePicture = async camera => {
    try {
      const options = {quality: 0.9, base64: false};
      const data = await camera.takePictureAsync(options);
      console.log(data);

      // setting the uri of the image
      setImage(data.uri);
    } catch (error) {
      console.warn(error);
    }
  };

  // if the image will be in the state then showing image else will show the camera
  return (
    <View style={styles.container}>
      {image ? (
        <View style={styles.preview}>
          <Image source={{uri: image, width: '100%', height: '90%'}} />
          <Button
            title="click new image"
            onPress={() => {
              setImage(null);
            }}
          />
        </View>
      ) : (
        <RNCamera
          style={styles.preview}
          type={RNCamera.Constants.Type.back}
          captureAudio={false}
          flashMode={RNCamera.Constants.FlashMode.on}
          androidCameraPermissionOptions={{
            title: 'Permission to use camera',
            message: 'We need your permission to use your camera',
            buttonPositive: 'Ok',
            buttonNegative: 'Cancel',
          }}
          androidRecordAudioPermissionOptions={{
            title: 'Permission to use audio recording',
            message: 'We need your permission to use your audio',
            buttonPositive: 'Ok',
            buttonNegative: 'Cancel',
          }}>
          {({camera, status}) => {
            if (status !== 'READY') return <PendingView />;
            return (
              <View
                style={{
                  flex: 0,
                  flexDirection: 'row',
                  justifyContent: 'center',
                }}>
                <TouchableOpacity
                  onPress={() => takePicture(camera)}
                  style={styles.capture}>
                  <Text style={{fontSize: 14}}> SNAP </Text>
                </TouchableOpacity>
              </View>
            );
          }}
        </RNCamera>
      )}
    </View>
  );
};

export default Camera;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: 'black',
  },
  preview: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  capture: {
    flex: 0,
    backgroundColor: '#fff',
    borderRadius: 5,
    padding: 15,
    paddingHorizontal: 20,
    alignSelf: 'center',
    margin: 20,
  },
});
