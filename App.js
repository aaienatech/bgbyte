import React from 'react';
import { View, Text } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import Upload_img from './src/screens/Upload_img';
import Splash from './src/screens/Splash';
import Onboardings from './src/screens/Onboardings';

const Stack = createStackNavigator();

const App = () => {
  return (
    <NavigationContainer>
    <Stack.Navigator initialRouteName="Splash" headerMode="none">
    <Stack.Screen name="Splash" component={Splash} />
      <Stack.Screen name="Onboardings" component={Onboardings} />
      <Stack.Screen name="Upload_img" component={Upload_img} />
    </Stack.Navigator>
  </NavigationContainer>
  )
}

export default App
